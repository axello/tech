---
layout: post
title: "Symphony of Fire at MCH2022"
subtitle: "Flamethrowers make some noise!"
date: 2022-09-13
img: symphony-of-fire.jpg
tags: [hardware, performace, music, energy]
---

I published four video’s from Symphony of Fire's flame thrower concert, taken at MCH2022

**Enjoy!**

First one: a medley of oldies and goodies

{% include youtubePlayer.html id="licbbjoCyhg" %}

Nr. two: a --for me-- unknown game song. If you know it, please tell me!

{% include youtubePlayer.html id="7aNOXNNx23A" %}

Nr. three: I believe Vivaldi's Four Seasons : Summer

{% include youtubePlayer.html id="BUicLuo3tqk" %}

Nr. four: AC/DC’s Thunderstruck!

{% include youtubePlayer.html id="lwD5NxlMTX4" %}
