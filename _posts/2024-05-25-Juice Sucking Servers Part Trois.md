---
layout: post
title: "Juice Sucking Servers, part trois"
subtitle: "new swift benchmarks"
date: 2024-05-25
img: electricity-hungry-computer-server3.jpg
tags: [hardware, swift, javascript, php, java, benchmarking, energy]
---
This is part three of the Juice Sucking Servers trilogy. The previous parts are [here](https://tech.phlux.us/Juice-Sucking-Servers) and [here](https://tech.phlux.us/Juice-Sucking-Servers-Part-Deux/).

## Introduction: patched wrk

More than 180 comments were given to my [initial post](). People investigated the issue, the Vapor framework was enhanced in [this PR](https://github.com/vapor/vapor/releases/tag/4.96.0) and the BigInt library I used was exchanged for the much quicker Numberick library. This is explained in my [second post]()

Several radars were filed by Wade Tregaskis and he created a _[summary post](https://wadetregaskis.com/swift-sucks-at-web-serving-or-does-it/)_ which was even longer than my initial post.

Wade asked me to re-run my initial swift benchmarks using the patched version of `wrk` and the new version of Vapor.

Due to time restraints I used version 4.96 of Vapor and not the newly released 4.100 version with the enhancements for running queries on NIO's Swift Concurrency global executor.

The initial results are much better. The weird issue where Vapor seemed to drop packets almost immediately is gone.

![swift succeeds for numberick and patched wrk](../assets/img/juice3/swift+patched-wrk-answers.png){: .center-image }

**succeeding answers within 2 seconds:**

![swift succeeds for numberick and patched wrk](../assets/img/juice3/overview-answers.png){: .center-image }

### Latency

Vapor can now succesfully compete with javascript and java on speed and number of transactions. 

![swift succeeds for numberick and patched wrk](../assets/img/juice3/overview-latency-all.png){: .center-image }

![swift succeeds for numberick and patched wrk](../assets/img/juice3/overview-latency-lower.png){: .center-image }


### Memory

Previously I measured memory ad-hoc using the `htop` program and manually entered the values in a spreadsheet. For these tests I wrote a script which samples the RSS memory field of `ps` after 25 seconds of starting a 30 seconds test.
The script is started from the benchmarking server using mqtt.

We see that the vapor+swift combination shines in the amount of _memory_ it uses for the same task compared to other frameworks. 

**memory usage of all processes combined, in MB:**

![swift succeeds for numberick and patched wrk](../assets/img/juice3/overview-memory.png){: .center-image }

### No power measurements

This time the network connection between the energy meter and the node-red instance was broken. Something to do with the new router probably. So I unfortunately do not have power measurements. I will add those later, but the measurements should not be significantly different than last time.

## Conclusion

With the increased limit of the acceptance queue, and a patched version of `wrk`, we can now conclude that swift is a good competitor speed-wise as a web application server.

Memory wise it blows all the other technologies away, using only 2.5% of the amount of memory that the java implementation needs, and 10% of node-js.

On modern web servers, memory is often the bottleneck and not CPU speed. 
[wadetregaskis wrote](https://forums.swift.org/t/standard-vapor-website-drops-1-5-of-requests-even-at-concurrency-of-100/71583/39)

> From my experience at LinkedIn (a very typical large, micro-services, Java server stack) peak memory usage is *by far* the dominanting factor in both capital costs and energy usage (DRAM is a lot more power-hungry than people think). Languages like Java require *huge* amounts of RAM in order to be performant [in non-trivial workloads], which is out of whack with the actual shape of hardware, so you end up maxing out the physical RAM capacity of every server and *still* using only a tiny fraction of its CPU capacity.
```

This makes swift a very viable server language.


## References

* [Make # of connections accepted per event loop cycle configurable, and raise the default](https://github.com/vapor/vapor/releases/tag/4.96.0)


## Thanks

Thanks to all the wonderful people at the [server swift forums](https://forums.swift.org/t/standard-vapor-website-drops-1-5-of-requests-even-at-concurrency-of-100/71583/1)

