---
layout: post
title: "Server Side Swift Conference 2024"
subtitle: "in London"
date: 2024-09-27
img: server-side-swift-2024.png
tags: [server, swift, conference]
---

On September 26 and 27, 2024 the fourth [Server Side Swift conference](https://www.serversideswift.info/) was held. This time in London.

The special thing about this conference, is that Apple was a main sponsor. Apple normally does not sponsor developer’s conferences outside wwdc. There were also about 20 people from Apple walking around, devs, managers, developer relations and even legal. Apparently it is a hot topic!

All available videos are referenced under this logo: 

[![Swift Server Conf logo](../assets/img/sss2024/ssclogo.png)](https://www.youtube.com/@swiftserverconf/featured)

# Day 1
Day one started with a talk by Tony Parker and Ben Cohen from Apple. Tony described how they were able to rewrite the Foundation library fully in swift, by replacing C++ code step-by-step. Making some code 100x faster!

* All C and ObjC Foundation is now rewritten in swift, with shims for ObjC and C
* Foundation is now fully open source
* better performance!, JSONDecode is 2-3x faster
* URLComponents: 250x faster!
* Papercuts initiative; painful small occurrences for new developers

![FormatStyle performance vs DateFormatter](../assets/img/sss2024/tonyparker-formatstyle.jpg)

Ben dropped the bomb on the swift-java interoperability: You can now include swift in java and vice-versa, using the swift-java library.

![Swift + Java interoperability](../assets/img/sss2024/ben-cohen-swiftjava.jpg)

[![Youtube](../assets/img/sss2024/ssclogo.png)](https://www.youtube.com/watch?v=wn6C_XEv1Mo)

---

Next Daniel Steinberg gave an insightful talk about swift macros. Explaining about freestanding and attached macros and several gotchas. And of course he elaborated on his _hate driven development_ paradigm.

![Swift Macros](../assets/img/sss2024/danielsteinberg-swiftmacros.jpg)

* remember to control-click on the preview macro
* he created an SFSymbol name macro
* remember to use the swift.ast-explorer.com website to examine the swift syntax tree
* freestanding macro, always starts with `#`, it replaces the macro with a string
* attached macros start with `@`, adds or supplements the code

[![Youtube](../assets/img/sss2024/ssclogo.png)](https://www.youtube.com/watch?v=MroBR2ProT0)

---

_One thing that is nice about the **Server Side Swift** conference are the many and long coffee breaks. Allowing you to meet and talk with all the other developers._

---

The next talk was about generating static websites using the [Toucan markdown to html converter](github.com/toucansites/toucan) in swift, by Tibor Bödecs. Toucan is different from Paul Hudson's `Publish`, in that it uses standard css, javascript and markdown, instead of swift code.

![Toucan markdown to html converter](../assets/img/sss2024/tiborbodecs-toucan.jpg)

[![Youtube](../assets/img/sss2024/ssclogo.png)](https://www.youtube.com/watch?v=FuTpnddLhpU)

---

After lunch a talk on how to ship a SAAS backend with Vapor, by Petr Pavlik. This was more a summing-up of all the tools he used.

![IndiePitcher](../assets/img/sss2024/petrpavlik-indiepitcher.jpg)

[![Youtube](../assets/img/sss2024/ssclogo.png)](https://www.youtube.com/watch?v=QFuZq9PHkTo)

---

The talk by Leo Dion about how to write a backend for an exercise app, complete with websockets integration with a website, directly from an Apple Watch was very interesting and complete. 

![Server Side Swift Workout in the Real World](../assets/img/sss2024/leodion-gbeat.jpg)

an architecture overview

![an architecture overview](../assets/img/sss2024/leodion-architecture.jpg)

[![Youtube](../assets/img/sss2024/ssclogo.png)](https://www.youtube.com/watch?v=4iB8s2fEmYc)

---

The talk by Mikaela Caron about how to save a file from an API was a bit…inconclusive. She had to ask the audience how to actually do it. Luckily there was a guy from AWS in the audience.
Her potential app Fruitful sounds promising though: an easy way to keep up with people you meet at conferences.

[![Youtube](../assets/img/sss2024/ssclogo.png)](https://www.youtube.com/watch?v=lHXjs3L2ads)

---

The final talk of the day was by Franz Busch from Apple’s Swift on Server Team, on how to leverage concurrency in a correct way in your app. e.g. how to cancel background tasks that are not needed anymore in a correct fashion. This had some excellent points.

* Avoid unstructured concurrency (dispatchqueue blocks)
* unstructured: cannot be cancelled
* do NOT create Tasks alone, store them and cancel them
* provide run methods
* default methods are 'nonisolated'
* handle graceful shutdown
* swift service lifecycle on github: `ServiceRunCycle`

![different tasks table](../assets/img/sss2024/franzbusch-differenttasks.jpg)

build-up of proper cancellable Task

![different tasks table](../assets/img/sss2024/franzbusch-code.jpg)

[![Youtube](../assets/img/sss2024/ssclogo.png)](https://www.youtube.com/watch?v=JmrnE7HUaDE)

---

In the evening there were drinks. The funniest moments were when [Georg Tuparev](https://tuparev.com/) impressed an NSLondon organiser with memories from his days using NeXTStep, and various meetings with Steve Jobs.

---

# Day 2

The second day started strong with a talk about serverless computing using swift lambdas on AWS, by Sébastian Stormacq from AWS Developer Relations. 
He also showed how to make the packages even smaller and more responsive using the new swift package [AWS Lambda Runtime](https://github.com/swift-server/swift-aws-lambda-runtime). And a VSCode plugin to more easily create the necessary [Serverless Application Model](https://github.com/aws/serverless-application-model) yaml config file.

![aws lambda](../assets/img/sss2024/sebastianstormacq-awslambda.jpg)

![aws lambda vscode plugin](../assets/img/sss2024/sebastianstormacq-lambdaplugin.jpg)

![aws demos](../assets/img/sss2024/sebastianstormacq-demos.jpg)

[![Youtube](../assets/img/sss2024/ssclogo.png)](https://www.youtube.com/watch?v=M1POAEPATFo)

---
Thomas Durand gave a quick introduction for iOS developers on what comprises a backend system. Talking about routes, middleware, authentication, best practices etc.

![Thomas Durand](../assets/img/sss2024/thomasdurand.jpg)

[![Youtube](../assets/img/sss2024/ssclogo.png)](https://www.youtube.com/watch?v=IAN0dmIAoeU)

---

Then there was a nice talk from Steven van Impe who created courseware material for SwiftWASM: Swift for Webassembly. As a lecturer at the HOGENT he explained the setup very clearly and had demo repos ready for people to start with.

![Steven van Impe](../assets/img/sss2024/stevenvanimpe.jpg)

![Toolchains and kits](../assets/img/sss2024/stevenvanimpe-setup.jpg)

![Demos](../assets/img/sss2024/stevenvanimpe-demos.jpg)

[![Youtube](../assets/img/sss2024/ssclogo.png)](https://www.youtube.com/watch?v=cJyNok8OAuE)

---

Just before lunch Adegboyega Olusunmade talked about a pub-quiz app he created. With a website build in vapor.

![Misadventures and adventures of Building a Trivia App with Vapor](../assets/img/sss2024/adegboyegaolusunmade.jpg)

[![Youtube](../assets/img/sss2024/ssclogo.png)](https://www.youtube.com/watch?v=QGO5ZuWM5pY)

---

### After lunch there were four shorter _lightning talks_. 

An interesting talk by Euan Harris from Apple, on how to publish to a cloud with only one step.

* streamline the containerisation process using swift package manager
* cross compiling: Swift-SDK -> cross compile to linux
* build your own Docker image using swift
* `podman`, to test containers with on the command line

![Swift to the cloud](../assets/img/sss2024/euanharris-swifttothecloud.jpg)

![Swift to the cloud](../assets/img/sss2024/euanharris-podman.jpg)

![Swift to the cloud](../assets/img/sss2024/euanharris-staticlinuxsdk.jpg)

[![Youtube](../assets/img/sss2024/ssclogo.png)](https://www.youtube.com/watch?v=9AaINsCfZzw)

---

Then a real life implementation of vapor from Vojtech Rylko from Cultured Code, for Things. They migrated their backend fully to swift and it is now handling 500 requests/second for the last 10 months. 

* resilience!
* "theory of operational information"
* mail - to Things
* At first they needed 50 Google compute instances
* they use swift-slim container image
* they use the swift prometheus package

![Things Cloud](../assets/img/sss2024/vojtechrylko-thingscloud.jpg)

They have one codebase, which can present itself as 4 different services:

* Services API
* Background
* Things Cloud API
* APNs Worker

They use **chaos engineering** on their internal development environment!

[![Youtube](../assets/img/sss2024/ssclogo.png)](https://www.youtube.com/watch?v=oJArLZIQF8w)
---

Student Babeth Velghe explained how to automatically create vapor code from an OpenAPI specification file (née JSONSpec).

![OpenAPI spec sample](../assets/img/sss2024/babethvelghe-openapi.jpg)

[![Youtube](../assets/img/sss2024/ssclogo.png)](https://www.youtube.com/watch?v=n1PRYVveLd0)

---
And finally Joannis Orlandos announced **Hummingbird 2**. A different web application technology, build from the ground up using swift 6 and async-await.

* Swift 6 web framework
* 100% structured concurrency
* no event loop futures
* Swift HTTP types
* up to 8x less code! (than Vapor?)

![Hummingbird 2 slide](../assets/img/sss2024/joannisorlandos-hummingbird2.jpg)
[![Youtube](../assets/img/sss2024/ssclogo.png)](https://www.youtube.com/watch?v=FHO_BfidQlQ)

---
### The last two talks were amazing.

A high-paced talk from Nick Lockwood titled “So you think you know swift”, with a cornucopia of swift gotchas and better code styles.
Nick wrote the SVGPath package and ShapeScript 

* for case let string in undundun 
* labels, for loops and if
* strings
* optionals
* enum associated values is also called tagged unions?

Great talk to watch again with a lot of low-level swift explanation.
![Struct with optional variable](../assets/img/sss2024/nicklockwood-optionalstruct.jpg)

![Terrible if case let syntax](../assets/img/sss2024/nicklockwood-goshdarnifcaseletsyntax.jpg)

![Double Switch](../assets/img/sss2024/nicklockwood-doubleswitch.jpg)

[![Youtube](../assets/img/sss2024/ssclogo.png)](https://www.youtube.com/watch?v=smkRzwANNQ8)

---

And the last talk by Cory Benfield from Apple‘s Swift on the Server team, on how Apple deploys swift on the server itself.
He talked about various anonymising algorithms, to power Private Cloud Compute.
It handles, among others:

* icloud keychain,
* shareplay
* noted
* private cloud compute

Lots of privacy acronyms mentioned, Hybrid Public Key encryption, KEM, KDF, AEAD. I might have gotten some wrong.

Idea: eliminate force-unwraps using a finite state machine ← look at that explanation again.

Private cloud compute handles 1000000 requests per second!

[![Youtube](../assets/img/sss2024/ssclogobw.png)]()

![](![]())
