---
layout: post
title: "Green Tech Hackathon"
subtitle: "in Amsterdam"
date: 2024-12-03
img: MaxPixel.net-Green-Tech-Hackathon-2024.jpg
tags: [swift, conference, architecture, javascript, java, php]
---

On December 3, 2024 I gave an inspiration talk at the precursory webinar of [RipeNCC's Green Tech Hackathon](https://labs.ripe.net/author/becha/announcing-the-green-tech-hackathon/).

Here are the slides.


[![Slides RipeNCC Green Tech Hackathon 2024](../assets/img/green-tech-hackathon-2024/overview.jpg)](https://gitlab.com/axello/tech/-/blob/master/assets/img/green-tech-hackathon-2024/Juice-Sucking-Servers-RipeNCC.pdf?ref_type=heads)
