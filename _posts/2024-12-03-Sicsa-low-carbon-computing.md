---
layout: post
title: "Sicsa Low Carbon Computing workshop"
subtitle: "remote, in Glasgow"
date: 2024-12-03
img: loco-splash-2024.png
tags: [swift, conference, architecture, javascript, java, php]
---

On December 3, 2024 I gave a lightning talk at the precursory webinar of [1st International Workshop on Low Carbon Computing](https://www.sicsa.ac.uk/loco/loco2024/), a hybrid event in Glasgow.

Here are the slides.


[![Slides RipeNCC Green Tech Hackathon 2024](../assets/img/sicsa-loco2024/overview.jpg)](https://gitlab.com/axello/tech/-/blob/master/assets/img/sicsa-loco2024/Juice-Sucking-Servers-LOCO2024.pdf?ref_type=heads)
